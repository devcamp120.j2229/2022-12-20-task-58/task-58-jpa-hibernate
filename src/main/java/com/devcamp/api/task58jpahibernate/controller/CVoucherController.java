package com.devcamp.api.task58jpahibernate.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.api.task58jpahibernate.model.CVoucher;
import com.devcamp.api.task58jpahibernate.repository.IVoucherRepository;

@CrossOrigin
@RestController
public class CVoucherController {
    @Autowired
    IVoucherRepository voucherRepository;
    
    @GetMapping("/vouchers")
    public ArrayList<CVoucher> getAllVouchers() {
        ArrayList<CVoucher> listVoucher = new ArrayList<>();

        voucherRepository.findAll().forEach(listVoucher::add);

        return listVoucher;
    }

    @GetMapping("/voucher2")
    public ResponseEntity<List<CVoucher>> getAllVouchers2() {
        try {
            ArrayList<CVoucher> listVoucher = new ArrayList<>();

            voucherRepository.findAll().forEach(listVoucher::add);
    
            if (listVoucher.size() == 0) {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            } else {
                //return new ResponseEntity<>(listVoucher, HttpStatus.OK);
                return ResponseEntity.ok().body(listVoucher);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
