package com.devcamp.api.task58jpahibernate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.api.task58jpahibernate.model.CVoucher;

public interface IVoucherRepository extends JpaRepository<CVoucher, Long> {
    
}
