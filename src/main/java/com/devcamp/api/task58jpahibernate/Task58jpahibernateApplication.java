package com.devcamp.api.task58jpahibernate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task58jpahibernateApplication {

	public static void main(String[] args) {
		SpringApplication.run(Task58jpahibernateApplication.class, args);
	}

}
